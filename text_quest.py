# WORLD_DESCRIPTIONS = [
#     "a dark, lush green forest",
#     "a dirt path along a bustling river",
#     "an open meadow full of butterflies and flowers",
#     "a white cliff overlooking the river as it plunges into a canyon"
# ]
# WORLD_CONNECTIONS = [
#     [-1, 1, 2, -1],
#     [-1, -1, 2, 0],
#     [1, -1, 3, 0],
#     [2, -1, -1, -1]
# ]

WORLD = {
    "starting_area": {
        "description": "a dark, lush green forest",
        "connections": {
            "west": None,
            "north": "river_path",
            "east": "open_meadow",
            "south": None
        }
    },
    "river_path": {
        "description": "a dirt path along a bustling river",
        "connections": {
            "west": None,
            "north": "river_gate",
            "east": "open_meadow",
            "south": "starting_area"
        }
    },
    "open_meadow": {
        "description": "an open meadow full of butterflies and flowers",
        "connections": {
            "west": "river_path",
            "north": None,
            "east": "canyon_cliff",
            "south": "starting_area"
        }
    },
    "canyon_cliff": {
        "description": "a white cliff overlooking the river as it plunges into a canyon",
        "connections": {
            "west": "open_meadow",
            "north": None,
            "east": None,
            "south": None
        }
    },
    "river_gate": {
        "description": "a worn, dirt path which curves away from the river, ending at a large stone wall",
        "connections": {
            "west": None,
            "north": "the_gate",
            "east": None,
            "south": "river_path"
        }
    },
    "the_gate": {
        "description": "a small wooden door in the stone wall",
        "connections": {
            "west": None,
            "north": None,
            "east": None,
            "south": "river_gate"
        }
    }
}

# WORLD_ITEMS = {
#     "starting_area": [
#         "pocket_knife"
#     ],
#     "canyon_cliff": [

#     ]
# }

game_over = False
player_position = "starting_area"
# player_items = [
#     "small_key"
# ]


def print_position():
    print("\nYou are standing in " +
          WORLD[player_position]["description"] + "\n")


def print_directions():
    here = WORLD[player_position]
    west = here["connections"]["west"]
    north = here["connections"]["north"]
    east = here["connections"]["east"]
    south = here["connections"]["south"]

    if west is not None:
        print("- To the west, you see " + WORLD[west]["description"])
    if north is not None:
        print("- To the north, you see " + WORLD[north]["description"])
    if east is not None:
        print("- To the east, you see " + WORLD[east]["description"])
    if south is not None:
        print("- To the south, you see " + WORLD[south]["description"])


while not game_over:
    print_position()
    print_directions()

    here = WORLD[player_position]
    west = here["connections"]["west"]
    north = here["connections"]["north"]
    east = here["connections"]["east"]
    south = here["connections"]["south"]

    moved = False
    while not moved:
        command = input("\nWhich direction will you go? ")
        if command.lower() == "west" and west is not None:
            player_position = west
            moved = True
        elif command.lower() == "north" and north is not None:
            player_position = north
            moved = True
        elif command.lower() == "east" and east is not None:
            player_position = east
            moved = True
        elif command.lower() == "south" and south is not None:
            player_position = south
            moved = True
        elif command.lower() == "quit":
            moved = True
            game_over = True
            print("Goodbye")
        else:
            print("Can't do that")
